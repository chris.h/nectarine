import networkx as nx
import itertools
import pandas as pd
import numpy as np
from NECTARINE.NeuroDP_framework import *

def func_timer(f):
    import time

    def wrapper(*args, **kwargs):
        t1 = time.process_time_ns()
        result = f(*args, **kwargs)
        end = time.process_time_ns()-t1
        print(f"{f} took {end}ns")
        return result, end
    return wrapper


@func_timer
def calc_sig_cand_idxs(met_res):
    # met res is an array that holds the results for each snapshots
    # iterate over array and find nodes that are only increasing or decreasing
    num_reg   = len(met_res[0])
    last_snap_idx = len(met_res)-1
    idxs = np.zeros((num_reg), dtype=np.intc)
    for res_idx in range(len(met_res[0])):
        #print(f"res_idx: {res_idx}")
        for snap_idx in range(len(met_res)-1):
            #print(f"snap_idx: {snap_idx}")
            curr_res = met_res[snap_idx][res_idx][1]#pos 0 is node number, pos 1 is met res
            if idxs[res_idx] == 0:
                #print(f"res_idx == 0")
                # first run or no trend yet
                if curr_res < met_res[last_snap_idx][res_idx][1]:
                    #print("curr_res < met_res+1")
                    idxs[res_idx] = snap_idx+1
                elif curr_res > met_res[last_snap_idx][res_idx][1]:
                    #print("curr_res > met_res+1")
                    idxs[res_idx] = -1 * (snap_idx+1)
            elif idxs[res_idx] > 0:
                #print(f"res_idx > 0")
                # rising run
                if curr_res < met_res[snap_idx-1][res_idx][1]:
                    idxs[res_idx] = snap_idx+1
                if curr_res > met_res[last_snap_idx][res_idx][1]:
                    #print("curr_res > met_res+1")
                    idxs[res_idx] = snap_idx+1
            else:
                # prev was larger, falling run
                #print(f"res_idx < 0")
                if curr_res > met_res[snap_idx-1][res_idx][1]:
                    idxs[res_idx] = -(snap_idx+1)
                if curr_res < met_res[last_snap_idx][res_idx][1]:
                    #print("curr_res < met_res+1")
                    idxs[res_idx] = -(snap_idx+1)
    print(f"idx: {idxs}")
    return idxs

def calc_sig_nodes(met_res, sig_level):
    node_cands = calc_sig_cand_idxs(met_res)[0]
    sig_nodes = [0] * len(node_cands)
    max_snap = len(met_res)-1
    #0 is not sig
    #i>0 is risig sig from snap i on
    #i<0 is falling sig from snap i on
    for idx, cand in enumerate(node_cands):
        if cand > 0:
            # rising
            snp_idx = cand-1
            lower = met_res[snp_idx][idx][1]
            higher = met_res[max_snap][idx][1]
            if higher - lower > sig_level:
                sig_nodes[idx] = 1
        if cand < 0:
            snp_idx = (-cand)-1
            higher = met_res[snp_idx][idx][1]
            lower = met_res[max_snap][idx][1]
            if higher - lower > sig_level:
                sig_nodes[idx] = -1
    print(sig_nodes)
    return sig_nodes
    
                    
def test_calc_sig_cand_idx():
    dummy_res = np.array([[1,2,3,4],
                          [4,3,2,1],
                          [3,1,3,1],
                          [2,2,1,1],
                          [2,2,2,1]], dtype=np.intc)
    dummy_res = dummy_res.T
    print(dummy_res.shape)
    print(dummy_res[0][1])
    expected = [1, -1, -3, -1, -1]
    print(calc_sig_cand_idxs(dummy_res))
    assert(calc_sig_cand_idxs(dummy_res)) == expected
        
        
def intersect_FC_SC(FC, SC):
    R = FC.copy()
    R.remove_nodes_from(n for n in FC if n not in SC)
    R.remove_edges_from(e for e in FC.edges if e not in SC.edges)
    return R

def create_graphs_FC(patient, connectivity):
    con_graph=create_graph_from_tvb(connectivity)
    best_FC_idx = patient.find_best_parameters_per_simulation()
    graphs = []
    for snap in patient.snapshots:
        sim_data = snap.simulation.data
        FC_idx = best_FC_idx[snap.id]
        FC = compFC(sim_data[:,0,:,FC_idx])
        FC = nx.Graph(FC)
        pruned_FC = intersect_FC_SC(FC, con_graph)
        graphs.append(pruned_FC)
    return graphs
        
def max_weight(graph):
    return max(nx.get_edge_attributes(graph, "weight").values())
        
def min_weight(graph):
    return min(nx.get_edge_attributes(graph, "weight").values())
    
def min_weight_filter(graph, e_filter):
    def f_func(x):
        return x > e_filter
    return min(filter(f_func, nx.get_edge_attributes(graph, "weight").values()))
    
def normalize_sc(patient):
    for snap in  patient.snapshots:
        (size_x, size_b) = snap.connectivity.weights.shape
        assert size_x == size_b

        #Diangonal to zero    
        np.fill_diagonal(snap.connectivity.weights, 0)

        #Normalize by the max value
        max_val = snap.connectivity.weights.max()
        min_val = snap.connectivity.weights.min()
    
    for i in range(size_x):
        for j in range(size_x):            
            snap.connectivity.weights[i][j] = (snap.connectivity.weights[i][j] - min_val) / (max_val - min_val)
    

def normalize(l):
    ma=max(l)
    mi=min(l)
    return list(map(lambda x: (x-mi)/(ma-mi), l))

def load_matix_from_df(regions, data):
    matrix = np.empty((0, regions), float)
    for line in data:
        line = line.split('\t')
        line = np.fromiter(map(float, line), dtype=np.float64)
        matrix = np.vstack((matrix, line))
    return matrix

def patient_from_import(p_id, regions, snapshots, model, sim_path, extra_data):
    print(f"snaps:{snapshots}")
    print(f"sim_path:{sim_path}")
    try:
        con = lab.connectivity.Connectivity.from_file("connectivity_" + str(regions) + ".zip")
    except:
        print(f"can't find connectivity file for {regions} regions")
        return
    p_id = ""
    patient = Patient(id=p_id)
    patient.snapshots = []
    for key in snapshots:
        print(key)
        df = pd.read_csv(snapshots[key])
        df_cort_left = df.loc[(df['side'] == "L")].reset_index(drop=True)
        df_cort_right= df.loc[(df['side'] == "R")].reset_index(drop=True)
        node_values = {}
        edge_values = {}
        # for key_ed in extra_data:
        #     if extra_data[key_ed]['name'] is not None:
        #         name = extra_data[key_ed]['name']
        #         if extra_data[key]['typ'] == 'Text' or extra_data[key_ed]['typ'] == 'Float':
        #             # value is to be attached to a node
        #             node_values[name] = df[name]
        #         if extra_data[key_ed]['typ'] == 'List of Floats':
        #             # value is to be attached to the edges
        #             edge_values[name] = df[name]
        weights = load_matix_from_df(regions, df['weights'])
        lengths = load_matix_from_df(regions, df['lengths'])
        s = Snapshot(id='A'+str(key), connectivity=Connectivity(weights=weights,
                                                                 lenghts=df['lengths'],
                                                                 regions=df['regions']))
        patient.snapshots.append(s)
    normalize_sc(patient)
    patient.load_simulations(sim_path, model=model)
    print(patient)
    print('ok')
    return patient, con


def nxgraph_from_snapshot(snapshot):
    # expects a snapshot from a patient
    weights = snapshot.connectivity.weights
    weights = [x for xs in weights for x in xs]
    num_nodes = len(snapshot.connectivity.regions)
    edges = list(itertools.permutations(range(0,num_nodes), 2))
    w_edges = [(a, b, c) for ((a, b), c) in zip(edges, weights)]
    g = nx.Graph()
    g.add_weighted_edges_from(w_edges)
    return g

def get_max_weight(snapshots):
    return max([s.connectivity.weights.max() for s in snapshots])

def get_min_weight(snapshots):
    return min([s.connectivity.weights.min() for s in snapshots])

def get_num_snaps(patient):
    return len(patient.snapshots)

def get_num_regions(connectivity):
    return len(connectivity.centres)


def test_nx():
    w_edges = [(1, 2, 0.4), (1, 4, 0.5), (1, 5, 0.4),
             (2, 3, 0.3),
             (3, 5, 0.1),
            ]
    g = nx.Graph()
    g.add_weighted_edges_from(w_edges)
    assert(len(g.edges)) == 5
    assert(g.degree[5]) == 2
    
def test_nxgraph_from_snapshot():
    from types import SimpleNamespace
    #populate fake snapshot
    snapshot = SimpleNamespace()
    snapshot.connectivity = SimpleNamespace()
    snapshot.connectivity.regions = range(0,96)
    snapshot.connectivity.weights = [[0.5] * (96)] * 96
    assert(len(snapshot.connectivity.regions)) == 96
    g = nxgraph_from_snapshot(snapshot)
    assert(len(g.nodes)) == 96
    for i in range(0,96):
        assert(nx.degree(g,i)) == 95

def mytest_patient_from_import():
    p_id='CBS05'
    num_regions = 96
    pd = './test_run/pre_processed/'
    sc_snaps={"1": pd+'snap_1.csv', 
              "2": pd+'snap_2.csv',
              "3": pd+'snap_3.csv',
              "4": pd+'snap_4.csv'}
    sim_model = 'rww_v2'
    sim_path = './test_run/simulations/'
    return patient_from_import(p_id, 96, sc_snaps,sim_model, sim_path, {})
    
    
def test_normalize():
    l = [1,2,3,4,5,6]
    assert(normalize(l)) == [0, 0.2, 0.4, 0.6,0.8,1]