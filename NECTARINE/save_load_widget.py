import ipywidgets as widgets
from NECTARINE.ipyfilechooser import FileChooser
import pickle

class Save_Load_Widget():
    def __init__(self, load_cb, save_cb):
        self.loaded_data = None
        self.load_cb = load_cb
        self.save_cb = save_cb
        self.load_widget = self.load_state()
        self.save_widget = None
        self.display = widgets.VBox([self.load_widget])
    
    
    def save_state(self):
        fc = FileChooser('.')
        def on_select(chooser):
            with open(chooser.selected, 'bw+') as save_file:
                save_data = self.save_cb()
                pickle.dump(save_data, save_file)
        fc.register_callback(on_select)
        l = widgets.Label("Save State")
        return widgets.VBox([l, fc])
                    
    
    def load_state(self):
        fc = FileChooser('.') # cute :)
        def on_select(chooser):
            with open(chooser.selected, 'br') as load_file:
                self.loaded_data = pickle.load(load_file)
                self.load_cb(self.loaded_data)
        fc.register_callback(on_select)
        l = widgets.Label("Load State")
        return widgets.VBox([l, fc])
                    
        
    def enable_save_widget(self):
        # this gets called by the gui after the aanlysis widget is created
        self.save_widget = self.save_state()
        children = list(self.display.children)
        children.insert(0, self.save_widget)
        self.display.children = children