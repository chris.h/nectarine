import plotly.graph_objects as go
import ipywidgets as widgets
import networkx as nx
import math
from NECTARINE.util import nxgraph_from_snapshot
from NECTARINE.NeuroDP_framework import *
from ipywidgets import GridspecLayout

from NECTARINE.brain_scatter_3d import BrainScatter3D
import NECTARINE.graph_analysis as ga
from NECTARINE.util import get_max_weight, get_min_weight, get_num_snaps, get_num_regions, create_graphs_FC, calc_sig_nodes

class AnalysisWidget(object):
    def __init__(self, patient, connectivity, update_out_cb, save_state=None):
        if save_state != None:
            self.patient = save_state['patient']
            self.current_snapshot = save_state['current_snap']
            self.connectivity = save_state['connectivity']
            self.curr_edge_filter = save_state['edge_filter']
            self.curr_node_filter = save_state['node_filter']
            self.curr_met_res = save_state['metric_res']
            self.sig_nodes = save_state['sig_nodes']
            self.brain_graphs = save_state['FC_graphs']
        else:
            self.patient = patient
            self.current_snapshot = 0
            self.connectivity = connectivity
            self.curr_edge_filter = 0
            self.curr_node_filter = list()
            self.curr_met_res=None
            self.sig_nodes = None
            self.brain_graphs = create_graphs_FC(self.patient, self.connectivity)
            
        for s in self.patient.snapshots:
            s.connectivity.region_labels = self.connectivity.region_labels
            s.connectivity.centres = self.connectivity.centres
        
        self.update_out_cb = update_out_cb
        self.brain_render = BrainScatter3D(self.connectivity, self.node_filter_cb)
        self.brain_render_out = widgets.HBox([self.brain_render.display])
        self.edge_filter = self.edge_filter(self.curr_edge_filter)
        self.graph_info_box = self.graph_info_box()
        self.select_node_metric = self.select_node_metric()
        grid = GridspecLayout(6,12, heigth='100px',  align_items="center", justify_items='center')
        grid[0,:] = self.snapshot_slider(self.current_snapshot)
        grid[1:5,1:8] = self.brain_render_out
        grid[5,:6] = self.edge_filter
        grid[5,6:12] = self.brainmesh_control()
        grid[2:4,8:12] = self.graph_info_box
        grid[4:5, 8:12] = self.select_node_metric
        self.display = grid 
        #initial "update":
        self.brain_render.filter_edges(self.curr_edge_filter, self.brain_graphs[self.current_snapshot])
        self.brain_render.apply_node_labels(self.curr_met_res, self.current_snapshot)
        self.brain_render.apply_node_colorscheme(self.curr_met_res, self.current_snapshot)
        self.brain_render.apply_sig_nodes(self.sig_nodes, self.current_snapshot)
        self.brain_render.mark_unmark_nodes(self.curr_node_filter)
        self.update_gui_out()
        
        
    def node_filter_cb(self, node_list):
        """
        callback for the brain scatter to pass along the choosen nodes
        """
        self.curr_node_filter = node_list
        self.update_gui_out()

    def graph_info_box(self):
        p_id = self.patient.id
        snap_num = self.current_snapshot
        l_p_id = widgets.Label(f"Patient ID: {p_id}")
        l_snap_num = widgets.Label(f"Current Snapshot: {str(snap_num)}")
        def set_snap_num(value):
            l_snap_num.value = f"Current Snapshot: {str(value)}"
        l_num_snaps = widgets.Label(f"Number of snapshots: {get_num_snaps(self.patient)}")
        l_num_regions = widgets.Label(f"Number of regions: {get_num_regions(self.connectivity)}")
        l_header = widgets.Label("Graph Info:")
        return widgets.VBox([l_header, l_num_regions, l_num_snaps])

    
    def edge_filter(self, init_filter):
        max_weight = 1.0
        min_weight = 0.0
        step = (max_weight - min_weight) / self.get_num_edges()
        def set_edge_filter(change):
            self.curr_edge_filter = change.new
            self.brain_render.filter_edges(self.curr_edge_filter, self.brain_graphs[self.current_snapshot])
        float_box = widgets.BoundedFloatText(value=init_filter, min=min_weight, max=max_weight)
        apply_button = widgets.Button(description="Apply Filter")
        def apply_filter(arg):
            filter=float_box.value
            self.curr_edge_filter=filter
            self.update_gui_out()
            self.brain_render.filter_edges(self.curr_edge_filter, self.brain_graphs[self.current_snapshot])
        apply_button.on_click(apply_filter)
        return widgets.VBox([float_box, apply_button])

    # def brain_render_box(self):
    #     return widgets.VBox([self.snapshot_slider() ,self.brain_render.display])

    def snapshot_slider(self, init_pos):
        num_snaps = len(self.patient.snapshots)
        if num_snaps < 2:
            return widgets.Label('This data contains only one snapshot')
        def set_snapshot(change):
            self.current_snapshot = change.new
            self.brain_render.filter_edges(self.curr_edge_filter, self.brain_graphs[self.current_snapshot])
            self.brain_render.apply_node_colorscheme(self.curr_met_res, self.current_snapshot)
            if self.curr_met_res != None:
                self.brain_render.apply_sig_nodes(self.sig_nodes, self.current_snapshot)
                self.brain_render.apply_node_labels(self.curr_met_res, self.current_snapshot)
        slider = widgets.IntSlider(min=0, max=num_snaps-1, value=init_pos)
        slider.observe(set_snapshot, 'value')
        lleft = widgets.Label('oldest')
        lright = widgets.Label('newest')
        return widgets.HBox([lleft, slider, lright])

    def select_node_metric(self):
        metric_list = []
        for e in ga.local_methods:
            metric_list.append(e["name"])
        w = widgets.RadioButtons(options=metric_list)
        rb=  widgets.RadioButtons(options=metric_list)
        return widgets.VBox([rb , self.run_button()])

    def run_button(self):
        def click(arg):
            current_node_metric = self.select_node_metric.children[0].value
            patient = self.patient
            coll = []
            for m in ga.local_methods:
                if m["name"] == current_node_metric:
                    for snap_idx in range(0, len(self.patient.snapshots)):
                        g = self.brain_graphs[snap_idx]
                        res = m['func'](graph=g, edge_filter=self.curr_edge_filter, node_list=self.curr_node_filter)
                        coll.append(res)
            self.curr_met_res = coll
            self.brain_render.apply_node_colorscheme(self.curr_met_res, self.current_snapshot)
            self.sig_nodes = calc_sig_nodes(coll, 0.05)
            self.brain_render.apply_sig_nodes(self.sig_nodes, self.current_snapshot)
            self.brain_render.apply_node_labels(self.curr_met_res, self.current_snapshot)
            self.update_gui_out()
        button = widgets.Button(description="Run")
        button.on_click(click)
        return button

    def get_num_edges(self):
        con = self.connectivity
        num = len(con.weights)
        return num
    
    def brainmesh_control(self):
        def click(change):
            if change.new == 0:
                self.brain_render.disable_brainmesh()
            else:
                self.brain_render.enable_brainmesh()
        button = widgets.ToggleButton(description='enable brain mesh', value=0)
        button.observe(click, names='value')
        def slider_change(change):
            if change.new == 0.0:
                self.brain_render.disable_brainmesh()
                button.value=0
            else:
                button.value=1
                self.brain_render.set_brainmesh_opacity(change.new)
        slider = widgets.FloatSlider(min=0, max=1, value=0.4)
        slider.observe(slider_change, names='value')
        return widgets.VBox([button, slider])
    
    
    def update_gui_out(self):
        out = {
            "FC_graphs": self.brain_graphs,
            "choosen_nodes": self.curr_node_filter,
            "analysis_results": self.curr_met_res,
            "FC_filter": self.curr_node_filter
        } # provides access to the tools output
        self.update_out_cb(out)
    
    # testing
    def toggle_edges(self, n, on_off):
        self.brain_render.display.update_traces(visible=on_off, selector=dict(name='edges_'+str(n)))
        
