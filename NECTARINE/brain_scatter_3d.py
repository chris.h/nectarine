import networkx as nx
import plotly.graph_objects as go
import numpy as np
import math as m

from scipy.spatial.transform import Rotation as R


from tvb.simulator.lab import *
from NECTARINE.brain_scatter import BrainScatter
from NECTARINE.util import func_timer, min_weight_filter, max_weight

class BrainScatter3D(BrainScatter):
    def __init__(self, connectivity, node_filter_cb):
        super().__init__(connectivity, node_filter_cb)
    
    def onclick_node(self, scatter, points, b):
        if len(points.point_inds) != 1:
            return
        try:
            self.node_list.remove(points.point_inds[0])
        except ValueError:
            self.node_list.append(points.point_inds[0])
        self.mark_unmark_nodes(self.node_list)
        self.node_filter_cb(self.node_list)
        
        
    def mark_unmark_nodes(self, node_list):
        colors = [(0,0,0,0)] * self.num_nodes()
        for i in range(len(colors)):
            if i in node_list:
                colors[i] = (1,0,1,1)
        self.display.update_traces(marker=dict(color=colors), selector=dict(name='node_marker'))

          
    def create_graph_render(self, connectivity):
        if connectivity is None:
            return go.FigureWidget()
        graph = self.graph
        labels = connectivity.region_labels
        
        num_nodes = len(graph.nodes)
        label_node = []
        node_size = 15

        for idx, name in enumerate(labels):
            label_node.append(str(idx) + "-" + name)

        self.node_labels = label_node

        spring_3D = nx.spring_layout(graph, dim=3, k=0.5)  # k regulates the distance between nodes

        x_nodes, y_nodes, z_nodes = [], [], []
        for node_idx in graph.nodes():
            x_, y_, z_ = connectivity.centres[node_idx]
            x_nodes.append(x_)
            y_nodes.append(y_)
            z_nodes.append(z_)

        node_color = [0]*self.num_nodes()
        node_marker = dict(
            symbol='circle',
            size=node_size,
            color=node_color,
            colorscale='Viridis',
            line=dict(color='black', width=2))

        # create a trace for the nodes
        trace_nodes = go.Scatter3d(
            name='nodes',
            x=x_nodes,
            y=y_nodes,
            z=z_nodes,
            mode='markers',
            marker=node_marker,
            text=label_node, hoverinfo='text')

        title = ""
        
        
        # generate edges
        # we generate 4 more graphs that hold the edges in different thickness as we cant change thickness of single edges
        self.edge_graphs = []
        for i in range(4):
            edges = self.gen_trace_edges(self.connectivity, i)
            self.edge_graphs.append(edges)
        
        cortex_shell = self.load_cortex_shell()
        self.marker_nodes = self.gen_node_marker(connectivity)
        data = [trace_nodes] + [self.marker_nodes] + self.edge_graphs + [cortex_shell]

        # we need to set the axis for the plot
        axis = dict(showbackground=False,
                    showline=False,
                    zeroline=False,
                    showgrid=False,
                    showticklabels=False,
                    title='')

        layout = go.Layout(title=title,
                           width=750,
                           height=725,
                           showlegend=True,
                           scene=dict(xaxis=dict(axis),
                                      yaxis=dict(axis),
                                      zaxis=dict(axis),
                                      ),
                           margin=dict(t=100),
                           hovermode='closest')
        fig = go.FigureWidget(data=data, layout=layout)
        fig.data[0].on_click(self.onclick_node)
        return fig
    
    def gen_node_marker(self, connectivity):
        graph = self.graph
        labels = connectivity.region_labels
        
        num_nodes = len(graph.nodes)
        node_size = 17

        spring_3D = nx.spring_layout(graph, dim=3, k=0.5)  # k regulates the distance between nodes

        x_nodes, y_nodes, z_nodes = [], [], []
        for node_idx in graph.nodes():
            x_, y_, z_ = connectivity.centres[node_idx]
            x_nodes.append(x_)
            y_nodes.append(y_)
            z_nodes.append(z_)

        node_colors = [(0,0,0,0)]  * num_nodes
        node_marker = dict(
            symbol='circle-open',
            size=node_size,
            color= node_colors,
            line=dict(color='black', width=0))
        node_marker = go.Scatter3d(
            name='node_marker',
            x=x_nodes,
            y=y_nodes,
            z=z_nodes,
            mode='markers',
            marker=node_marker,
            showlegend=False,
            text=['']*self.num_nodes(),
            hoverinfo="none")
        return node_marker
        
    def load_cortex_shell(self):
        face = surfaces.CorticalSurface.from_file()
        r = R.from_quat([0, 0, np.sin(np.pi/4), np.cos(np.pi/4)]) # 90 degrees
        vertices = np.matmul(r.as_matrix(), face.vertices.transpose()) #rotation
        vertices = vertices*1.1
        
        triangles = face.triangles.transpose()

        x = vertices[0]
        y = vertices[1]
        z = vertices[2]
        i = triangles[0]
        j = triangles[1]
        k = triangles[2]
        return go.Mesh3d(x=x, y=y, z=z, i=i, j=j, k=k, color='lightpink', visible=False, opacity=0.3, hoverinfo="none", showlegend=False)

        
    def gen_trace_edges(self, connectivity, weight_cat):
        # we create fully transparent edges and then update them with ilter edges
        # we need to create lists that contain the starting and ending coordinates of each edge.
        x_edges, y_edges, z_edges = [], [], []

        # create lists holding midpoints that we will use to anchor text
        xtp, ytp, ztp = [], [], []
        curr_graph = self.graph # we assume that on initial generation we can use the first graph
        
        width_lookup = {
            0: 1,
            1: 2,
            2: 4,
            3: 8
        }
        
        colors = []
        for a_idx, b_idx in curr_graph.edges:
            pos_node_a = connectivity.centres[a_idx]
            pos_node_b = connectivity.centres[b_idx]

            x_edges += [pos_node_a[0], pos_node_b[0], None]
            xtp.append(0.5 * (pos_node_a[0] + pos_node_b[0]))

            y_edges += [pos_node_a[1], pos_node_b[1], None]
            ytp.append(0.5 * (pos_node_a[1] + pos_node_b[1]))

            z_edges += [pos_node_a[2], pos_node_b[2], None]
            ztp.append(0.5 * (pos_node_a[2] + pos_node_b[2]))

        # create a trace for the edges
        trace_edges = go.Scatter3d(
            name="edges_"+str(weight_cat),
            x=x_edges,
            y=y_edges,
            z=z_edges,
            mode='lines',
            line=dict(color=[(1, 1, 1, 0)] * self.num_edges() , width=width_lookup[weight_cat]),
            text=[''] * self.num_edges(), hoverinfo='text')
        return trace_edges

    @func_timer
    def filter_edges(self, edge_filter, graph):
        widht_lookup = {
            0: 1,
            1: 2,
            2: 4,
            3: 8
        }
        
        def rev_weight_lookup(w):
            if w < 1:
                return 0
            if w < 2:
                return 1
            if w < 4:
                return 2
            else:
                return 3
        
        mi_weight = min_weight_filter(graph, edge_filter)
        ma_weight = max_weight(graph)
        
        def scale_weight(w):
            return (w-mi_weight)*(8-1) / (ma_weight - mi_weight) + 1
        
        cat_count = [0,0,0,0]
        num_nodes = graph.number_of_nodes()
        color=[(0,0,0,0)] * self.num_edges()
        graph_colors = {
            0: [(0,0,0,0)] * self.num_edges(),
            1: [(0,0,0,0)] * self.num_edges(),
            2: [(0,0,0,0)] * self.num_edges(),
            3: [(0,0,0,0)] * self.num_edges()
        }
        for x, y in graph.edges():
            if graph.adj[x][y]['weight'] > edge_filter:
                w = graph.adj[x][y]['weight']
                pos = x * num_nodes + y
                cat = rev_weight_lookup(scale_weight(w))
                cat_count[cat] += 1
                graph_colors[cat][pos] = (0,0,0,1)
                
        for idx, edge_graph in enumerate(self.edge_graphs):
            self.display.update_traces(line=dict(color=graph_colors[idx]), selector=dict(name='edges_'+str(idx)))
    
    def set_brainmesh_opacity(self, opacity):
        self.display.update_traces(opacity=opacity, selector=dict(type='mesh3d'))
        
    def disable_brainmesh(self):
        self.display.update_traces(visible=False, selector=dict(type='mesh3d'))
        
    def enable_brainmesh(self):
        self.display.update_traces(visible=True, selector=dict(type='mesh3d'))