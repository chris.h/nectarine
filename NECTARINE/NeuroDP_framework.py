#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
    NeuroDP allows you to do parameters space exploration on a simulation using customized brain map
    Author: Aarón Pérez Martín
    Contact:a.perez.martin@fz-juelich.de
    Organization: Forschungszentrum Jülich

'''
#

import pickle, os, sys, zipfile, json, seaborn as sns, math, io
import matplotlib.pyplot as plt, pandas as pd

#import xml.etree.ElementTree as ET

import plotly.graph_objects as go
import networkx as nx
import numpy as np

plt.rcParams.update({'font.size': 11})

import scipy.stats
from sklearn.cross_decomposition import CCA
from zipfile import ZipFile

# Version from file
#from PyUnicoreManager import *

# Version from Pip install
from pyunicoremanager.core import *

import tvb.simulator.lab as lab

import requests
#from openid_http_client.http_client import HttpClient
#from openid_http_client.auth_client.access_token_client import AccessTokenClient

# Common names for simulation
INPUT_NAME = "input_data.zip"
OUTPUT_NAME = "tavg_data"
PARAM_NAME = "params_data"

# Files name can not star with those characters
no_prefixs = ('.', '~', '#')

# Steps for simulations
#NVIDIA_SW = ["module load Python SciPy-Stack numba PyCUDA"]
NVIDIA_test = ["module load Python SciPy-Stack numba PyCUDA",
               "export CUDA_VISIBLE_DEVICES=0 ",
               "nvidia-smi"]
#JOB_PARAMS = {"Project": "slns", 'Resources': {"Queue": "develgpus", "Nodes": "1", "Runtime": "10m"}}

logss_cbs = set_logger("ndp.core")


class Type(IntEnum):
    # Different type of output file conversion
    CUDA = 0
    PYTHON = 1

##  UTILS ##
# -----------

def load_matrix_dataset(path_dataset, total_length, max_round, split_char=" ", skip_rows=[]):
    matrix = np.empty((0, total_length), float)
    with open(path_dataset, 'r') as f:
        line_num = 0
        for line in f:
            if line_num in skip_rows:
                continue
            raw = line.split(split_char)
            # Filters
            nums = filter(None, raw)  # no empty items

            x = np.round(np.fromiter(map(float, nums), dtype=np.float64), max_round)
            matrix = np.vstack((matrix, x))
            line_num += 1
    return matrix

def check_location(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
    return True

def check_format(string: str, format_str) -> bool:
    format_arr = [idx for idx, c in enumerate(format_str) if c == char]
    str_arr = [idx for idx, c in enumerate(str) if c == char]

def ploting(data, time=None):
    if time is not None:
        plt.plot(time, data, alpha=0.1)
    else:
        plt.plot(data, alpha=0.1)
    plt.show()

def get_sessions(user_folder):
    # TODO: Order of files is important
    _, sub_folders, _ = next(os.walk(user_folder))
    sub_folders = sorted(sub_folders)
    return [x for x in sub_folders if not x.startswith(no_prefixs)]

def get_files(user_folder):
    # TODO: Order of files is important
    print(user_folder)
    _, _, filenames = next(os.walk(user_folder))
    return sorted(filenames)

def LoadModel(filename, concept="model"):
    try:
        file = open(filename, 'rb')
        data = pickle.load(file)
        file.close()
        # (simsteps, states, nodes, paramscombi)
        logss_cbs.info("Loaded " + concept + " " + filename + str(data.shape))
        return data
    except Exception as e:
        logss_cbs.error(e)

def LoadModel_TVB(region):
    try:
        return lab.connectivity.Connectivity.from_file('connectivity_' + str(region) + '.zip')
    except Exception as e:
        logss_cbs.error(e)

# Calculates the Functional Connectivity
# NOTE: assuming no interaction means no connection. TODO assuming other point of views.
# dynamic FC, moving window
def compFC(timeSeries, if_nan=0):
    t_matrix = np.corrcoef(timeSeries.T)
    # Check NaNs
    index_NANs = np.isnan(t_matrix)
    t_matrix[index_NANs] = if_nan

    return t_matrix

# Calculates the degree of correlation between the Functional and the Structural Connectivity
def corrSCFC(SC, FCloc, round=None):
    if round:
        return np.round(np.corrcoef(FCloc.ravel(), SC.ravel())[0, 1], 6)
    else:
        return np.corrcoef(FCloc.ravel(), SC.ravel())[0, 1]

# Generates a plot of the Functional and Structural connectivity. Returns their correlation coefficient.
def plot_FCSC(SC, FCloc, user_id, to_save=None, quality=300):
    fig, ax = plt.subplots(ncols=2, figsize=(12, 3))

    # fig.suptitle("Title for whole figure", fontsize=16)
    sns.heatmap(FCloc, xticklabels='', yticklabels='', ax=ax[0], cmap='YlGnBu')  # coolwarm
    sns.heatmap(SC / SC.max(), xticklabels='', yticklabels='', ax=ax[1], cmap='YlGnBu')

    r = corrSCFC(SC, FCloc)
    if math.isnan(r):
        print("There is a NaN in the correlation")
    ax[0].set_title('simulated FC. \nSC-FC r = ' + str(r))
    ax[1].set_title('SC: ' + user_id)
    if to_save and check_location(to_save):
        plt.savefig(to_save + "/" + user_id + "_ScFc.jpg", dpi=quality, bbox_inches='tight')
    return r

def get_bestOrworst_n_elements(array, n_elements, oper=''):
    # sorted indices array
    sorted_index_array = np.argsort(np.array(array))

    # sorted array
    sorted_array = array[sorted_index_array]

    # find n largest value
    if oper == '-':
        return sorted_array[:n_elements], sorted_index_array[:n_elements]
    elif oper == '+':
        return sorted_array[-n_elements:], sorted_index_array[-n_elements:]
    return [], []

def get_bestOrworst_n_edges(matrix, num_edges, oper='', bidirectional=True, clear_diagonal=True):
    mat = np.array(matrix)  # avoid changes by reference

    coor_val = []
    # Replacing the hightest values by the worst, and viceverse
    if oper == '+':
        coor_val = np.argwhere(mat == np.amin(mat))  # .tolist()[0]
    elif oper == '-':
        coor_val = np.argwhere(mat == np.amax(mat))  # .tolist()[0]
    else:
        return []

    newValue = mat[coor_val[0][0], coor_val[0][1]]
    if clear_diagonal:
        # setting up the diangonal with the opposite value we are looking for
        np.fill_diagonal(mat, newValue)

    tuple_list_oper = []

    # Stop condition
    dim_x, dim_y = mat.shape
    index = 0
    total_access = dim_x * dim_y
    # Calculate max/min, replace by the mean, and calculate again until fullfil the num_edges
    while (len(tuple_list_oper) < num_edges and total_access > 0):  # dim_x >=0 and dim_y >=0 #
        if oper == '+':
            oper_tuples = np.argwhere(mat == np.amax(mat)).tolist()

        elif oper == '-':
            oper_tuples = np.argwhere(mat == np.amin(mat)).tolist()
        else:
            oper_tuples = []

        index += 1
        if len(oper_tuples) > 0:
            sublist = oper_tuples[0]
            # print(sublist, "-->", tuple_list_oper, sublist in tuple_list_oper)
            if not sublist in tuple_list_oper:
                tuple_list_oper.append(sublist)
                mat[sublist[0], sublist[1]] = newValue
            total_access -= 1
    return tuple_list_oper  # [:num_edges]

def get_edgesfrom_FCS(snapshots, index_max_corr_SC_FC):
    # Dicctionaries where loaded in order
    key_list = list(index_max_corr_SC_FC.keys())
    number_of_items = len(key_list)

    dic_FC2_1 = {}
    idx = 0

    # Getting a pair of FCs each iteration
    for first_id, second_id in zip(key_list[:], key_list[1:]):

        # best FC of each snapshot
        FC_1 = compFC(snapshots[idx].simulation.data[:, 0, :, index_max_corr_SC_FC[(first_id)]])
        FC_2 = compFC(snapshots[idx + 1].simulation.data[:, 0, :, index_max_corr_SC_FC[second_id]])
        idx += 1

        if np.isnan(FC_1).any() or np.isnan(FC_2).any():
            print("There are NaNs in the FCs matrices")
            return []

        # Matrix result
        FC_2_1 = FC_2 - FC_1

        # Normalize
        (size_x, size_y) = FC_2_1.shape
        assert size_x == size_y

        max_val = FC_2_1.max()
        min_val = FC_2_1.min()

        for i in range(size_x):
            for j in range(size_x):
                FC_2_1[i][j] = (FC_2_1[i][j] - min_val) / (max_val - min_val)

        name = second_id + "_" + first_id
        dic_FC2_1[name] = FC_2_1

    return dic_FC2_1

def compare_connectivity_affected_nodes(conn_1, conn_2, abs_factor=0.08):
    list_of_nodes_affected = []

    if conn_1.shape == conn_2.shape:
        rows, cols = conn_1.shape
        # going through the upper triangle of the connec. matrix, assuming symmetry
        for row in range(rows):
            for col in range(row + 1, cols):
                a = conn_1[row][col]
                b = conn_2[row][col]

                if a != 0.0 and b != 0.0 and abs(a - b) > abs_factor:
                    list_of_nodes_affected.append(row)
                    list_of_nodes_affected.append(col)

        # removing repeated
        list_of_nodes_affected = list(set(list_of_nodes_affected))
    return list_of_nodes_affected

# Compare snapshot of the brain and extract what are the nodes become stronger and weaker depending on the weight on their edges
def compare_connectivity_affected_edges(Matrix_1, Matrix_2, threshold_H=0.08):
    nodes_plus = []
    nodes_minus = []
    rows, cols = Matrix_1.shape

    # For discriminate changes at the end
    m_max = [[0.0] * rows for _ in range(cols)]
    m_min = [[0.0] * rows for _ in range(cols)]

    all_list = [Matrix_1, Matrix_2]

    threshold_H = 0.08
    threshold_L = 0.0001

    list_plus = []
    list_minus = []

    # Get differences
    for row in range(rows):
        for col in range(row + 1, cols):
            # Getting a pair every iterarion
            for m_1, m_2 in zip(all_list, all_list[1:]):

                # compare brain information, max and min
                a = m_1[row][col]
                b = m_2[row][col]
                num = round(abs(a - b), 6)

                # Loosing connectivity
                if a >= 0 and a > b and num < threshold_L:
                    list_minus += [(row, col)]  # change for edges
                    m_min[row][col] = num

                # New connectivity
                elif b > 0 and a < b and num > threshold_H:
                    list_plus += [(row, col)]  # change for edges
                    m_max[row][col] = num

    max_value = max([max(l) for l in m_max])
    min_value = min([min(l) for l in m_min])
    print("max:", max_value, "min:", min_value)

    # removing repeated
    list_plus = list(set(list_plus))
    list_minus = list(set(list_minus))

    return list_plus, list_minus

def reduce_edges_by_weight(edges, factor=0.1):
    return list([(a, b) for a, b, attrs in edges(data=True) if attrs["weight"] <= factor])

def create_graph_from_tvb(connectivity, factor_edge=0.0):
    return create_graph(connectivity.region_labels, connectivity.weights, connectivity.centres, factor_edge)

def create_graph(labels, weights, points=[], factor_edge=0.0):
    graph = nx.Graph()

    # points = tvb_connectivity.centres

    # Adding nodes
    for idx, label in enumerate(labels):  # connectivity.region
        if len(points)>0:
            graph.add_node(idx, name=label, position= points[idx])
        else:
            graph.add_node(idx, name=label)  # , position= connectivity.centres[idx] , connectivity.labels[idx])

    # Adding edges, using upper triangle of the matrix
    rows, cols = weights.shape
    for row in range(rows):
        for col in range(row + 1, cols):
            if weights[row][col] > factor_edge:
                graph.add_edge(row, col, weight=weights[row][col])
    return graph

# ---------------------------

class Plotter():
    @staticmethod
    def operation(graph, centres, edge_weights, labels, region_selected, edges_selected, color="red"):
        # Selected Nodes and its edges
        # -----------------------------
        x_selected, y_selected, z_selected = [], [], []

        # For testing:
        # print("Loop nodes",color,type(region_selected),region_selected)
        # print("Loop edges",color,type(edges_selected),edges_selected)

        label_selected_node = []
        for idx in region_selected:
            # each brain region has a 3D point associated
            x_, y_, z_ = centres[idx]
            x_selected.append(x_)
            y_selected.append(y_)
            z_selected.append(z_)
            label_selected_node.append(str(idx)+"-"+labels[idx])

        # Add Nodes
        # ------------------------------------------------
        trace_selected_nodes = go.Scatter3d(x=x_selected, y=y_selected, z=z_selected,
                                            mode='markers',
                                            marker=dict(symbol='circle',
                                                        size=10,
                                                        color=color,  # "rgba(0,24,168, 0.5)",
                                                        line=dict(color='black', width=1.5)),
                                            text=label_selected_node, hoverinfo='text')
        xtp_2, ytp_2, ztp_2 = [], [], []
        etext_2 = []
        x_edges_2, y_edges_2, z_edges_2 = [], [], []

        labels_edges = []

        # Add Edges, it needs two nodes and its properties
        # ------------------------------------------------
        for a_idx, b_idx in edges_selected:  # graph.edges:

            pos_node_a = centres[a_idx]
            pos_node_b = centres[b_idx]

            # For testing:
            # print("edges_from:",a_idx,pos_node_a, pos_node_a[0])
            # print("edges___to:",b_idx,pos_node_b, pos_node_b[0])
            labels_edges.append(str((a_idx, b_idx)))

            x_edges_2 += [pos_node_a[0], pos_node_b[0], None]
            xtp_2.append(0.5 * (pos_node_a[0] + pos_node_b[0]))

            y_edges_2 += [pos_node_a[1], pos_node_b[1], None]
            ytp_2.append(0.5 * (pos_node_a[1] + pos_node_b[1]))

            z_edges_2 += [pos_node_a[2], pos_node_b[2], None]
            ztp_2.append(0.5 * (pos_node_a[2] + pos_node_b[2]))

            etext_2 = [f'weight={w}' for w in edge_weights]

        trace_selected_edges = go.Scatter3d(x=x_edges_2, y=y_edges_2, z=z_edges_2,
                                            mode='lines',
                                            line=dict(color=color, width=8),
                                            text=labels_edges, hoverinfo='none')
        # hoverinfo='text')
        trace_selected_weights = go.Scatter3d(x=xtp_2, y=ytp_2, z=ztp_2,
                                              mode='markers',
                                              marker=dict(color=color, size=5),
                                              # set the same color as for the edge lines
                                              text=etext_2, hoverinfo='text')

        return trace_selected_nodes, trace_selected_edges, trace_selected_weights

    @staticmethod
    def plot_3d(graph, connectivity, centres, labels, dict_region_affected, dict_edges_affected, title_txt="",
                dic_factors=None):
        G = graph

        edges = G.edges()
        edge_weights = range(len(G.nodes) + 1)

        Num_nodes = len(G.nodes)
        
        label_node = []
        
        for idx, name in enumerate(labels):
            label_node.append(str(idx)+"-"+name)

        # Reduce number of edges by value
        if dic_factors:
            to_remove = reduce_edges_by_weight(edges, dic_factors["edges_until_val"])
            G.remove_edges_from(to_remove)
            # print(nx.info(G))

        spring_3D = nx.spring_layout(G, dim=3, k=0.5)  # k regulates the distance between nodes

        x_nodes, y_nodes, z_nodes = [], [], []
        for node_idx in graph.nodes():
            x_, y_, z_ = centres[node_idx]
            x_nodes.append(x_)
            y_nodes.append(y_)
            z_nodes.append(z_)

        # we need to create lists that contain the starting and ending coordinates of each edge.
        x_edges, y_edges, z_edges = [], [], []

        # create lists holding midpoints that we will use to anchor text
        xtp, ytp, ztp = [], [], []

        ####

        for a_idx, b_idx in graph.edges:
            pos_node_a = centres[a_idx]
            pos_node_b = centres[b_idx]

            x_edges += [pos_node_a[0], pos_node_b[0], None]
            xtp.append(0.5 * (pos_node_a[0] + pos_node_b[0]))

            y_edges += [pos_node_a[1], pos_node_b[1], None]
            ytp.append(0.5 * (pos_node_a[1] + pos_node_b[1]))

            z_edges += [pos_node_a[2], pos_node_b[2], None]
            ztp.append(0.5 * (pos_node_a[2] + pos_node_b[2]))

        etext = [f'weight={w}' for w in edge_weights]
        """
        trace_weights = go.Scatter3d(x=xtp, y=ytp, z=ztp,
                                     mode='markers',
                                     marker=dict(color='rgb(125,125,125)', size=1),
                                     # set the same color as for the edge lines
                                     text=etext, hoverinfo='text')
        """
        # create a trace for the edges
        trace_edges = go.Scatter3d(
            x=x_edges,
            y=y_edges,
            z=z_edges,
            mode='lines',
            line=dict(color='gray', width=1),
            text=labels, hoverinfo='text')
        # hoverinfo='none')

        # create a trace for the nodes
        trace_nodes = go.Scatter3d(
            x=x_nodes,
            y=y_nodes,
            z=z_nodes,
            mode='markers',
            marker=dict(symbol='circle',
                        size=6,
                        color='orange',
                        line=dict(color='black', width=0.5)),
            text=label_node, hoverinfo='text')  # text

        # Include the traces we want to plot and create a figure
        # data = [trace_edges, trace_nodes, trace_weights, trace_selected_nodes, trace_selected_edges, trace_selected_weights]

        title = ""
        data = [trace_nodes, trace_edges]
        for snap, configuration in dict_region_affected.items():
            title = str(snap) + ":" + title_txt
            for color, nodes_affected in configuration.items():
                if str(snap) in dict_edges_affected.keys():
                    edges_affected = dict_edges_affected[str(snap)][str(color)]

                    # For testing:
                    # print(color, "Nodes", nodes)
                    # print(color, "Edges", edges)
                    _nodes, _edges, _edges_weights = Plotter.operation(graph, centres, edge_weights,
                                                               labels, nodes_affected, edges_affected, color)
                    data += [_nodes, _edges]  # , _edges_weights]
                else:
                    _nodes, _edges, _edges_weights = Plotter.operation(graph, centres, edge_weights,
                                                               labels, nodes_affected, [], color)
                    data += [_nodes]  # , _edges, _edges_weights]

        # ----------------
        # we need to set the axis for the plot
        axis = dict(showbackground=False,
                    showline=False,
                    zeroline=False,
                    showgrid=False,
                    showticklabels=False,
                    title='')

        layout = go.Layout(title=title,
                           width=750,
                           height=725,
                           showlegend=False,
                           scene=dict(xaxis=dict(axis),
                                      yaxis=dict(axis),
                                      zaxis=dict(axis),
                                      ),
                           margin=dict(t=100),
                           hovermode='closest')
        fig = go.Figure(data=data, layout=layout)

        fig.show()
    @staticmethod
    def plot_freedom_degree(graph, user_id, snap_id="", to_save=None, info=False):
        list_degrees = []

        if graph:
            for node in graph.nodes:
                list_degrees.append(graph.degree(node))
            plt.bar(graph.nodes, list_degrees)
            plt.xlabel('Brain Nodes')
            plt.ylabel('Freedom degree of nodes')
            plt.grid()

            folder = str(to_save + "/images")
            if to_save and check_location(to_save):
                plt.savefig(to_save +
                            "/" + str(user_id) +
                            "_"+  str(snap_id) +"_freedom_degree.jpg", dpi=300, bbox_inches='tight')

            plt.show()
            if info:
                print(nx.info(graph))

        else:
            print("No graph")

    @staticmethod
    def plot_2D_normal(graph, region_selected=[]):
        pos = nx.spring_layout(graph)

        nx.draw(graph, pos, with_labels=True)
        nx.draw_networkx_edges(graph, pos, edgelist=region_selected, edge_color='r', width=5)
        plt.show()

    @staticmethod
    def plot_2D_circular(graph, region_selected=[]):
        pos = nx.circular_layout(graph)

        nx.draw_circular(graph, node_color='y', edge_color="#909090", with_labels=True)

        nx.draw_networkx_edges(graph, pos, edgelist=region_selected, edge_color='r', width=5)
        print(region_selected)

        # plt.rcParams['figure.figsize'] = [14, 8]
        plt.show()

    @staticmethod
    def plot_variety_intime(serie_list,  user_id, to_save=None, quality=300):
        serie = np.arange(1, 49)

        # print(serie.shape,len(patient.snapshots[0].connectivity.regions))

        fig, ax = plt.subplots(figsize=(20, 5))

        colors = ["royalblue", "deepskyblue", "springgreen", "limegreen"]
        regions_1 = serie[:48]  # serie_list[0].connectivity.labels[:48]
        regions_2 = serie[48:]  # serie_list[0].connectivity.labels[48:]

        ### Right side ######
        for idx, snap in enumerate(serie_list):
            ax.scatter(serie, np.array(snap.connectivity.regions[:48]), label=str(snap.id), c=colors[idx])

        # ax.set_title('Right side of the brain')
        ax.set_xticks(np.arange(0, 49))
        # ax.set_xticklabels(regions_1)
        ax.set_xticklabels(np.arange(0, 49))
        ax.set_axisbelow(True)
        ax.set_xlabel('Brain Nodes')
        ax.set_ylabel('Lost mass volume')

        plt.setp(ax.get_xticklabels(), visible=True)
        plt.tick_params(axis='x', which='major', labelsize=11)

        plt.xlim([0, 49])
        plt.legend()
        # plt.xticks(rotation=45, ha='right')
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.grid()

        if to_save and check_location(to_save):
            plt.savefig(to_save + "/" + user_id + "_dots_right.jpg", dpi=quality, bbox_inches='tight')
        plt.show()

        fig, ax = plt.subplots(figsize=(20, 5))
        ### Left side ######
        for idx, snap in enumerate(serie_list):
            ax.scatter(serie, np.array(snap.connectivity.regions[48:]), label=str(snap.id), c=colors[idx])

        # ax.set_title('Left side of the brain')
        ax.set_xticks(np.arange(0, 49))
        # ax.set_xticklabels(regions_2)
        ax.set_xticklabels(np.arange(0, 49))
        ax.set_axisbelow(True)
        ax.set_xlabel('Brain Nodes')
        ax.set_ylabel('Lost mass volume')

        plt.tick_params(axis='x', which='major', labelsize=11)

        plt.xlim([0, 49])
        plt.legend()
        # plt.xticks(rotation=45, ha='right')
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.grid()
        if to_save and check_location(to_save):
            plt.savefig(to_save + "/" + user_id + "_dots_left.jpg", dpi=300, bbox_inches='tight')
        plt.show()

    @staticmethod
    def plot_variety_boxplot(serie_list, user_id, to_save=None, quality=300):
        data_r, data_l = np.array([]), np.array([])
        """
        #TODO:refactorize
        for snap in serie_list:
            data_r = np.array(data_r, snap.connectivity.regions[:48])
            data_l = np.array(data_l, snap.connectivity.regions[48:])
        """
        data_r = np.array([serie_list[0].connectivity.regions[:48],
                           serie_list[1].connectivity.regions[:48],
                           serie_list[2].connectivity.regions[:48],
                           serie_list[3].connectivity.regions[:48]])
        data_l = np.array([serie_list[0].connectivity.regions[48:],
                           serie_list[1].connectivity.regions[48:],
                           serie_list[2].connectivity.regions[48:],
                           serie_list[3].connectivity.regions[48:]])

        df_r = pd.DataFrame(data_r)  # , columns = serie_list[0].connectivity.labels[0:48])
        ax = df_r.plot.box(figsize=(20, 3))

        # ax.set_title('Right side of the brain')
        ax.set_xticks(np.arange(0, 49))
        ax.set_xlabel('Brain Nodes')
        ax.set_ylabel('Lost mass volume')
        # plt.xticks(rotation=45, ha='right')
        if to_save and check_location(to_save):
            plt.savefig(to_save + "/" + user_id + "_boxplot_right.jpg", dpi=quality, bbox_inches='tight')
        plt.show()

        df_l = pd.DataFrame(data_l)  # , columns = serie_list[0].connectivity.labels[48:96])
        ax = df_l.plot.box(figsize=(20, 3))
        # ax.set_title('Left side of the brain')
        ax.set_xlabel('Brain Nodes')
        ax.set_ylabel('Lost mass volume')
        ax.set_xticks(np.arange(0, 49))

        # plt.xticks(rotation=45, ha='right')
        if to_save and check_location(to_save):
            plt.savefig(to_save + "/" + user_id + "_boxplot_left.jpg", dpi=quality, bbox_inches='tight')
        plt.show()

    @staticmethod
    def plot_variety_stacked_bars(serie_list):
        regions = serie_list.connectivity.labels[48:]
        width = 0.35
        fig, ax = plt.subplots(figsize=(20, 3))
        """
        TODO: Refactorize this
        """
        serie1 = np.array(serie_list[0].connectivity.regions[48:])
        serie2 = np.array(serie_list[1].connectivity.regions[48:])
        serie3 = np.array(serie_list[2].connectivity.regions[48:])
        serie4 = np.array(serie_list[3].connectivity.regions[48:])

        p1 = ax.bar(regions, serie1, width, label='serie1')
        p2 = ax.bar(regions, serie2, width, bottom=(serie1), label='serie2')
        p3 = ax.bar(regions, serie3, width, bottom=(serie1 + serie2), label='serie3')
        p4 = ax.bar(regions, serie4, width, bottom=(serie1 + serie2 + serie3), label='serie4')

        ax.axhline(0, color='grey', linewidth=0.8)
        ax.set_ylabel('Scores')
        ax.set_title('Scores by group and gender')
        ax.set_xticks(np.arange(48))
        ax.set_xticklabels(regions)
        ax.legend()

        # Label with label_type 'center' instead of the default 'edge'
        # ax.bar_label(p1, label_type='center')

        # ax.set_title('Right side of the brain')
        plt.xticks(rotation=45, ha='right')
        plt.show()
        
    @staticmethod
    def plot_heatmap(patient, axis):
        plt.style.use("seaborn")
        for snap in patient.snapshots:
            _, _, _, param_list = snap.simulation.data.shape
            output_shape = snap.simulation.output_params.shape
            param_values_coefs = list()
            SC = snap.connectivity.weights


            for param_set in range(param_list):
                FC = compFC(snap.simulation.data[:, 0, :, param_set])
                corrcoef = corrSCFC(SC, FC)
                if not math.isnan(corrcoef):
                    param_values_coefs.append(corrcoef)
                else:
                    param_values_coefs.append(0.001)


            ax_x_name= list(axis.keys())[0]
            ax_y_name= list(axis.keys())[1]

            values = np.reshape(param_values_coefs, (axis[ax_x_name],axis[ax_y_name])).T

            lbl_0 = sorted(list(set(snap.simulation.output_params[:,0])))
            lbl_1 = sorted(list(set(snap.simulation.output_params[:,1])))

            plt.figure(figsize=output_shape)
            heat_map = sns.heatmap(values, xticklabels=lbl_0, yticklabels=lbl_1, linewidth = 1 , annot = True, fmt=".6f")
            heat_map.set(title= str(snap.id)+"\n HeatMap Pearson Coef FC-SC",
                         xlabel=ax_x_name,
                         ylabel=ax_y_name,)
            plt.show()

            plt.style.use('default')

class DataAnalysis(object):

    def model_CCA(X, Y, components=1, decimals=15):
        try:
            cca = CCA(n_components=components)
            X_, Y_ = cca.fit_transform(X, Y)

            corrcoef, p_value = scipy.stats.pearsonr(np.ravel(X_), np.ravel(Y_))
            corrcoef = np.round(corrcoef, decimals)
            p_value = np.round(p_value, decimals)

            logss_cbs.info("CCA: X_" + str(X_.shape) + ", Y_" + str(Y_.shape) +
                           " Coef.Pear: corrcoef " + str(corrcoef) + ", p_value " + str(p_value))
            return corrcoef, p_value
        except Exception as err:
            logss_cbs.error(err)
            return None, None

class Simulation():
    def __init__(self, name, location, input_params=None):
        self.name = name
        self.data = None
        self.location = location
        self.input_params = input_params  # dict: coupling(min,max), speed(min, max), etc
        self.output_params = None  # parameters used

class Simulator():
    def __init__(self, authentication: Authentication, language: Type):
        self.language = language
        self.auth = authentication
        self.sim = None

    def pick_dict(self, file_location, obj_to_store):
        try:
            with open(file_location, 'wb') as pickleFile:
                pickle.dump(obj_to_store, pickleFile)

        except Exception as err:
            logss_cbs.error(err)

class Connectivity():
    def __init__(self, weights, lenghts, regions, labels="", to_save=None):
        self.weights = weights
        self.tract_lengths = lenghts
        self.regions = regions
        self.labels = labels

class Snapshot():
    def __init__(self, id, connectivity):
        self.id = id
        self.connectivity = connectivity
        self.simulation = list()  # class Simulation
        self.best_dict = None  # param, pearson, etc
        self.graph = None

    def info(self):
        if self.graph:
            print(nx.info(self.graph))

class Patient():
    def __init__(self, id=0):
        self.id = id
        self.snapshots = list()
        self.info = dict()

    def load_patient_from_KG(self, path_info, weights_name, lenghts_name, regions=0, decimals=6):
        try:
            if regions == 0:
                raise Exception("Please, specifiy the number of regions")

            logss_cbs.info("Loading data from: " + str(path_info))
            weights = load_matrix_dataset(path_dataset=path_info + weights_name,
                                          total_length=regions,
                                          max_round=decimals)
            tract_lengths = load_matrix_dataset(path_dataset=path_info + lenghts_name,
                                                total_length=regions,
                                                max_round=decimals)
            # return weights_matrix, lenghts_matrix
            regions = Fake.fake_matrix(regions=regions)

            self.snapshots.append(Snapshot(id=self.id, connectivity=Connectivity(weights, tract_lengths, regions)))
        except Exception as err:
            logss_cbs.error(err)
            return None, None

    def load_one_session_with_format(self, content_str, format_str, char=os.sep, regions=68, decimals=6):
        try:
            dict_content, dict_main = {}, {}

            # check if content follows the format
            a = [idx for idx, c in enumerate(format_str) if c == char]
            b = [idx for idx, c in enumerate(content_str) if c == char]
            if len(a) != len(b):
                return None

            array_cnt = content_str.split(char)
            array_ids = format_str.split(char)

            # content and format can change, they are dynamically filled
            dict_main.fromkeys(array_ids)
            for idx, val in enumerate(array_cnt):
                pos_found = content_str.rfind(val)
                dict_content[val] = content_str[0:pos_found + len(val)]
                dict_main[array_ids[idx]] = dict_content[val]

            self.id = dict_main["user"].split(char)[-1]

        except Exception as err:
            logss_cbs.error("Format must be [folder/User_id/session_id/SC/weights.txt]", err)

    def load_sessions(self, location, subfolder="", regions=68, decimals=6, split_char=" ", fill_gaps=True):
        try:
            # location/ user_id / sesion_id / data
            root = os.path.join(location, self.id)
            sessions = get_sessions(root)
            self.snapshots = list()
            for session in sessions:
                if len(subfolder) > 0:
                    relative_path = os.path.join(root, session, subfolder)
                else:
                    relative_path = os.path.join(root, session)
                files = get_files(relative_path)

                w, t, r = None, None, None
                for file in files:
                    folder = os.path.join(relative_path, file)
                    if "weights" in file:
                        w = load_matrix_dataset(path_dataset=folder, total_length=regions, max_round=decimals,
                                                split_char=split_char)
                    elif "tract_lengths" in file:
                        t = load_matrix_dataset(path_dataset=folder, total_length=regions, max_round=decimals,
                                                split_char=split_char)
                    elif "regions" in file:
                        r = load_matrix_dataset(path_dataset=folder, total_length=regions, max_round=decimals,
                                                split_char=split_char)

                if not isinstance(w, np.ndarray) or w.shape == (0, 0):
                    if fill_gaps:
                        w = Fake.fake_matrix(regions=68)
                        logss_cbs.info(str(session) + " - No weights")
                    else:
                        logss_cbs.info(str(session) + " - Filled weights")
                if not isinstance(t, np.ndarray) or len(t) == 0:
                    if fill_gaps:
                        t = Fake.fake_array(regions=68)
                        logss_cbs.info(str(session) + " - Filled tract_lengths")
                    else:
                        logss_cbs.info(str(session) + " - not tract_lengths")
                if not isinstance(r, np.ndarray) or len(t) == 0:
                    if fill_gaps:
                        r = Fake.fake_array(regions=68)
                        logss_cbs.info(str(session) + " - Filled regions")
                    else:
                        logss_cbs.info(str(session) + " - not regions")
                self.snapshots.append(
                    Snapshot(id=session, connectivity=Connectivity(w, t, r)))

            logss_cbs.info("Total sessions: " + str(len(sessions)))
        except Exception as err:
            logss_cbs.error("Format must be [folder/User_id/session_id/SC/weights.txt]", err)

    def load_simulations(self, local_subdir, model="rwongwang"):
        simulations_folder = get_files(local_subdir)

        if len(self.snapshots) == 0:
            logss_cbs.info("No snapshots list")
        count_sims = 0
        for snapshot in self.snapshots:

            sim_params_name = PARAM_NAME + "_" + model + "_" + str(snapshot.id)
            sim_params = os.path.join(local_subdir, sim_params_name)

            snapshot.sims = list()
            for sim_name in simulations_folder:
                if sim_name == OUTPUT_NAME + "_" + model + "_" + str(snapshot.id):
                    sim_path = os.path.join(local_subdir, sim_name)
                    obj_sim = Simulation(name=sim_name, location=local_subdir)
                    obj_sim.data = LoadModel(sim_path)
                    obj_sim.output_params = LoadModel(sim_params, concept="parameters")

                    snapshot.simulation = obj_sim
                    count_sims += 1
        logss_cbs.info("Loaded simulations " + str(count_sims))

    def find_best_parameters_per_simulation(self):
        # TODO: remove prints
        references = {}
        for snap in self.snapshots:
            _, _, _, param_list = snap.simulation.data.shape

            param_values_coefs = list()
            SC = snap.connectivity.weights

            print(snap.id)

            for param_set in range(param_list):
                try:
                    FC = compFC(snap.simulation.data[:, 0, :, param_set])  # [time_serie, states, regions, params]
                    # TODO:
                    # corrcoef ,_ = DataAnalysis.model_CCA(SC,FC, components=68, decimals=6)

                    corrcoef = corrSCFC(SC, FC)
                    if not math.isnan(corrcoef):
                        param_values_coefs.append(corrcoef)

                except Exception as ex:

                    print("Error: " + str(ex))

            maxvalue = max(param_values_coefs)
            idx = param_values_coefs.index(maxvalue)
            params = snap.simulation.output_params[idx]

            references[str(snap.id)] = idx
            # print(" -", param_values_coefs)
            print(" - max corr.coef.: ", round(maxvalue, 6), ", index:" + str(idx) + "", ",params:", params)
        return references

    def compare_sims_timeseries(self, width=12.5, height=10.5):
        fig, axs = plt.subplots(len(self.snapshots), 1, figsize=(width, height), squeeze=False)

        for idx, snap in enumerate(self.snapshots):
            axs[idx, 0].plot(snap.simulation.data[:, 0, :, 0])

    def compare_sims_scfc(self):

        for idx, snap in enumerate(self.snapshots):
            w = snap.connectivity.weights
            d = snap.simulation.data
            user_id_snap = str(self.id) + "_" + str(snap.id)
            plot_FCSC(w, compFC(d[:, 0, :, 0]), str(snap.id), user_id_snap)

class Fake():

    @staticmethod
    def fake_matrix(min_val=0.0, max_val=250.0, regions=68):
        return np.random.randint(min_val, max_val, (regions, regions))

    @staticmethod
    def fake_array(min_val=0.0, max_val=250.0, regions=68):
        return np.random.randint(min_val, max_val, regions)

class TVB_pySimulation():
    def __init__(self, **args):
        self.model = args.get('model', lab.models.ReducedWongWang())
        self.connectivity = args.get('connectivity', LoadModel_TVB("68"))
        self.coupling = args.get('coupling', lab.coupling.Linear(a=np.array([1. / 68.])))
        self.integrator = args.get('integrator', lab.integrators.EulerDeterministic(dt=0.1))
        self.monitor = args.get('monitor', [lab.monitors.TemporalAverage(period=10.0)])
        self.simulation_length = args.get('simulation_length', 4000)
        self.conduction_speed = args.get('conduction_speed', None)
        self.time, self.data = None, None

    def run(self):
        self.sim = lab.simulator.Simulator(
            model=self.model,
            connectivity=self.connectivity,
            coupling=self.coupling,
            integrator=self.integrator,
            monitors=self.monitor,
            simulation_length=self.simulation_length,
            conduction_speed=self.conduction_speed)
        self.sim.configure()

        self.sim.history.buffer[:] = 0.0
        self.sim.current_state[:] = 0.0

        (self.time, self.data), = self.sim.run()

    def plot(self):
        plt.plot(time=self.time, data=self.data)
        plt.show()

class HPC_cudaSimulation():
    def __init__(self, auth,**args):
        
        clean_job_storages=args.get('clean_job_storages', True)
        env_from_user = args.get('env_from_user', {})
        verbose = args.get('verbose', False)
        
        auth = Authentication(token=auth.token, access=auth.access, server=auth.server)
        
        self.env = Environment_UNICORE(auth=auth, env_from_user=env_from_user)                
        self.unicore = PyUnicoreManager(environment=self.env, clean_job_storages=clean_job_storages, verbose=verbose)
        self.result = None
        self.executed_job = None
    
    def compile_model(self, tvb_loc, jobarguments, model="rwongwang", input_params=""):
        steps = [self.env.job_info["loadmodules"],
                 "export PYTHONPATH=$PYTHONPATH:" + tvb_loc + "/tvb_library",
                 "cd " + tvb_loc + "/tvb_library/tvb/rateML",
                 "python3 XML2model.py -m " + model + " -l python",
                 "python3 XML2model.py -m " + model + " -l cuda"]
        #RateML does a comparation between CPU and GPU calculation
        
        logss_cbs.info("Compiling model: " + str(model))
        self.executed_job, self.result = self.unicore.one_run(steps=steps, parameters=jobarguments)

    def run(self, tvb_loc, jobarguments, model="rwongwang", input_params="", id=""):
        steps = [self.env.job_info["loadmodules"],
                 "export PYTHONPATH=$PYTHONPATH:" + tvb_loc + "/tvb_library",
                 "cd " + tvb_loc + "/tvb_library/tvb/rateML/run",
                 #"python3 model_driver.py -m " + model + " -w " + input_params]
                 "python3 model_driver_"+model+".py -m " + model + " -w " + input_params]                 

        logss_cbs.info("Doing a simulation: " + str(model) + ", simulation: " + str(id))
        self.executed_job, self.result = self.unicore.one_run(steps=steps, parameters=jobarguments)
        
    #def make_simulations(self, patient, tvb_location, local_subdir, remote_upload_subdir, remote_download_subdir, sim_params="", model="rwongwang"):
    def make_simulations(self, patient, folder_dic, sim_params="", model="rwongwang"):
        count_sims = 0
        # For each snapshot a simulation/parameter sweep is done.
        if len(patient.snapshots) == 0:
            logss_cbs.warning("There is not any snapshot")
            return
        
        # Execution mnust be on the Computing nodes, needs to remove the 'Job type'
        #JobArguments_LoginNode = dict(self.env.job_info["JobArguments"])         
        JobArguments_CompNode = dict(self.env.job_info["JobArguments"])   
        if 'Job type' in JobArguments_CompNode:
            JobArguments_CompNode.pop('Job type')

        # self.sim = HPC_cudaSimulation(self.auth, clean_job_storages=True)  # To avoid UNICORE issues of availability
        self.compile_model(model=model, tvb_loc=folder_dic["remote_tvb_folder"], jobarguments=JobArguments_CompNode , input_params=sim_params)
        if not self.result:
            return None

        for snapshot in patient.snapshots:

            # Creating a dictionary file as input for the simulation
            dict_files = {}
            dict_files["weights.txt"] = snapshot.connectivity.weights
            dict_files["tract_lengths.txt"] = snapshot.connectivity.tract_lengths
            dict_files[
                "regions.txt"] = snapshot.connectivity.regions  # np.full(snapshot.connectivity.weights.shape, 1.)
            print(snapshot.connectivity.weights.shape, snapshot.connectivity.tract_lengths.shape,
                  len(snapshot.connectivity.regions))

            # TODO: extend the files
            # dict["best_params"] = []
            # dict["input_params"] = input_params  # snapshot.simulation.input_params # dict: coupling:[min,max], speed:[min, max], etc

            # Prepare Zip of input data
            if "input_data" in sim_params:
                StorageManager.store_files_fromDict(where=folder_dic["local_folder_simulations"], id=snapshot.id, dic=dict_files, toZip=True)

                # Input data
                # sim_input_local_name = INPUT_NAME + "_" + model + "_" + str(snapshot.id)
                sim_input_path = os.path.join(folder_dic["local_folder_simulations"], str(snapshot.id), INPUT_NAME)

                # self.pick_dict(file_location=sim_input_path, obj_to_store=dict)
                filesToUpload = [[sim_input_path, os.path.join(folder_dic["remote_download_subfolder"], INPUT_NAME)]] # Download folder is when simulations needs the input
            
                if not self.upload_files(filesToUpload):
                    return None
                
            self.run(model=model, tvb_loc=folder_dic["remote_tvb_folder"], jobarguments=JobArguments_CompNode, input_params=sim_params, id=snapshot.id)
            if not self.result:
                logss_cbs.error("Please review the simulation parameter")
                return None

            # output data
            sim_name = OUTPUT_NAME + "_" + model + "_" + str(snapshot.id)
            sim_path = os.path.join(folder_dic["local_folder_simulations"], sim_name)
            # Output param combinnation
            sim_params_name = PARAM_NAME + "_" + model + "_" + str(snapshot.id)
            sim_params_path = os.path.join(folder_dic["local_folder_simulations"], sim_params_name)

            # Downloading timeSeries + parameterSweep values
            filesToDownload = [[sim_path, os.path.join(folder_dic["remote_download_subfolder"], OUTPUT_NAME)],
                               [sim_params_path, os.path.join(folder_dic["remote_download_subfolder"], PARAM_NAME)]]
            logss_cbs.info("Downloading: "+ str(filesToDownload))
            if not self.download_result(filesToDownload):
                return None

            obj_sim = Simulation(name=sim_name, location=folder_dic["local_folder_simulations"])
            obj_sim.data = LoadModel(sim_path)
            obj_sim.output_params = LoadModel(sim_params_path, concept="parameters")

            snapshot.simulation = obj_sim
            count_sims += 1

        logss_cbs.info("Loaded simulations:" + str(count_sims))
        return patient.snapshots

    def test_GPUs_access(self):
        self.executed_job, self.result = self.unicore.one_run(steps=NVIDIA_test, parameters=self.env.job_info["JobArguments"])

    def download_result(self, files):
        return self.unicore.downloadFiles(files)

    def upload_files(self, files):
        return self.unicore.uploadFiles(files)

class KG_Manager():
    def __init__(self, token, **args):
        self.token = token
        self.req = self.check_credentials(self.token)
        self.has_access = True
        #if not self.req.status_code == 200:
        #    self.has_access = False
        #    print("Error code: ", self.req.status_code)
        self.query = None

    def check_credentials(self, doi=""):
        session = requests.Session()
        session.headers['Authorization'] = self.token
        session.headers['accept'] = 'application/json'
        # DOI is optional
        return session.get(
            "https://kg.humanbrainproject.eu/query/minds/core/dataset/v1.0.0/TVBtest/instances?databaseScope=RELEASED&DOI=" + str(
                doi))

    def kg_query(self, doi):
        if not self.has_access:
            return
        """ TODO: find another library similar to "openid_http_client"
        
        auth_client = AccessTokenClient(self.token)
        print("auth_client",auth_client)
        http_client = HttpClient("https://kg.humanbrainproject.eu/query", "", auth_client=auth_client)

        url = "{}/{}/instances?databaseScope=RELEASED&{}".format("minds/core/dataset/v1.0.0",
                                                                 "TVBtest", "DOI=" + doi)
        self.query = http_client.get(url)
        if self.query:
            for key, value in self.query["results"][0].items():
                key_word = key.split("/")[-1]
                if key_word == "name":
                    print("Title:", value)
            print("Database:", self.query["databaseScope"])
        """
    def download_file(self, prefix, where):
        remote_header = "https://kg.ebrains.eu/proxy/export?container=" + self.query["results"][0][
                                                                              'https://schema.hbp.eu/myQuery/container_url'][
                                                                          :-4]  # removing "v1.0"
        remote_url = remote_header + prefix
        print("remote_header",remote_header)
        print("remote_url",remote_url)
        local_filename = remote_url.split('/')[-1]
        path_local_filename = os.path.join(where, local_filename)
        with requests.get(remote_url, stream=True) as r:
            r.raise_for_status()
            with open(path_local_filename, 'wb') as file:
                for chunk in r.iter_content(chunk_size=8192):
                    # If you have chunk encoded response uncomment "if" and set chunk_size parameter to None.
                    if chunk:
                        file.write(chunk)
        return path_local_filename, remote_url

    def download_range(self, num_from, num_to, prefix, where):
        for n in np.arange(num_from, num_to + 1, 1):
            web_prefix = prefix + "/" + str(n)
            filename, _ = self.download_file(prefix=web_prefix, where=where)
            self.unzip_file(local_folder=where, local_filename=filename)

    def unzip_file(self, local_folder, local_filename):
        files = list()
        if zipfile.is_zipfile(local_filename):
            with zipfile.ZipFile(local_filename, 'r') as zip_ref:
                zip_ref.extractall(local_folder)
                print("Extracted ", local_filename, ":", len(zip_ref.namelist()), "files")

class StorageManager():

    @staticmethod
    def store_files_fromDict(where: str, id: str, dic: dict, toZip=True):
        if len(where) > 0:
            folder = os.path.join(where, id)

            for key, value in dic.items():
                if not os.path.exists(folder):
                    os.makedirs(folder)
                file_path = os.path.join(folder, key)
                np.savetxt(file_path, value, delimiter='\t')  # separator compatible with TVB reader

            if toZip:
                StorageManager.zip_files_fromDict(folder, dic)

    @staticmethod
    def zip_files_fromDict(where: str, dic: dict):
        zipObj = ZipFile(os.path.join(where, INPUT_NAME), 'w')
        for file in dic.keys():
            file_path = os.path.join(where, file)
            zipObj.write(file_path, arcname=file)
        zipObj.close()

    @staticmethod
    def read_files_fromZip_to_nparray(filename: str, array_shape):
        dict = {}
        with ZipFile(filename, "r") as zipfile:
            for subfile in zipfile.namelist():
                if not os.path.isdir(subfile):
                    with io.TextIOWrapper(zipfile.open(subfile), encoding='utf-8') as file:
                        dict[str(subfile)] = np.fromstring(file.read(), dtype=float, sep='\t').reshape(array_shape)
                        print(dict[str(subfile)].shape)
        return dict

#################