import networkx as nx

class Graph_Analysis:
    def __init__(self, graph, edge_filter, node_list):
        self.graph       = graph
        self.node_list   = node_list
        self.edge_filter = edge_filter


    def filter_node(self, node):
        """
        if the node is in self.node_list return True, else False
        """
        if len(self.node_list) == 0:
            return True
        if node in self.node_list:
            return True
        else:
            return False

    def filter_edge(self, e1, e2):
        """
        return True if node weight is smaller than self.edge_filter
        """
        if self.edge_filter is None:
            return True
        try:
            w = self.graph[e1][e2]['weight']
        except KeyError:
            print("can't access edge property 'weight'")
        return w > self.edge_filter

def get_subgraph(graph, edge_filter, node_list):
    ga = Graph_Analysis(graph, edge_filter, node_list)
    sg = nx.subgraph_view(graph, filter_node=ga.filter_node, filter_edge=ga.filter_edge)
    # print(sg.edges)
    # print(sg.nodes)
    # print(sg.adj)
    return sg

def transitivity():
    return nx.transitivity(get_subgraph())

def shortest_path_length():
    pass

def global_efficiency():
    return nx.global_efficiency(get_subgraph())

def weighted_degree(graph, edge_filter, node_list):
    g = get_subgraph(graph, edge_filter=edge_filter, node_list=node_list)
    deg = dict(g.degree(weight='weight'))
    res = [(a,b/max(deg.values())) for (a,b) in zip(deg.keys(), deg.values())]
    return res

def closeness_centrality(graph, edge_filter, node_list):
    g = get_subgraph(graph, edge_filter, node_list)
    res = nx.closeness_centrality(G=g, distance='weight')
    return [(a,b) for (a,b) in zip(res.keys(), res.values())]

def clustering_coefficient(graph, edge_filter, node_list):
    g = get_subgraph(graph, edge_filter, node_list)
    res = nx.clustering(G=g, weight='weight')
    res = [(a,b) for (a,b) in zip(res.keys(), res.values())]
    print(res)
    return res

def eigenvector_centrality(graph, edge_filter, node_list):
    g = get_subgraph(graph, edge_filter, node_list)
    res = nx.eigenvector_centrality_numpy(G=g, weight='weight')
    res = [(a,b) for (a,b) in zip(res.keys(), res.values())]
    return res


def betweenness_centrality(graph, edge_filter, node_list):
    g = get_subgraph(graph, edge_filter, node_list)
    res = nx.betweenness_centrality(G=g, weight='weight')
    res = [(a,b) for (a,b) in zip(res.keys(), res.values())]
    return res

def current_flow_betweenness_centrality(graph, edge_filter, node_list):
    g = get_subgraph(graph, edge_filter, node_list)
    res = nx.current_flow_betweenness_centrality(G=g, weight='weight')
    res = [(a,b) for (a,b) in zip(res.keys(), res.values())]
    return res


def harmonic_centrality(graph, edge_filter, node_list):
    #not normalized
    g = get_subgraph(graph, edge_filter, node_list)
    res = nx.harmonic_centrality(G=g, distance='weight')
    res = [(a,b) for (a,b) in zip(res.keys(), res.values())]
    return res

global_methods = [
    {"name": "Transitivity", "func": transitivity},
    {"name": "Shortest Path Length", "func": shortest_path_length},
    {"name": "Global Efficincy", "func": global_efficiency}]
local_methods = [
    {"name": "Weighted Degree", "func": weighted_degree},
#    {"name": "Harmonic Centrality", "func": harmonic_centrality},
    {"name": "Current Flow Betweenness Centrailty", "func": current_flow_betweenness_centrality},
    {"name": "Betweenness Centrality", "func": betweenness_centrality},
    {"name": "Eigenvector Centrality", "func": eigenvector_centrality},
    {"name": "Closeness Centrality", "func": closeness_centrality},
    {"name": "Clustering Coefficient", "func": clustering_coefficient}
]