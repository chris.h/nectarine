from NECTARINE.NeuroDP_framework import *
import plotly.graph_objects as go
from NECTARINE.util import func_timer
import copy


class BrainScatter(object):
    def __init__(self, connectivity, node_filter_cb):
        self.connectivity = connectivity
        self.graph = self.generate_graph(connectivity)
        self.display = self.create_graph_render(connectivity)
        self.node_list = list()
        self.node_filter_cb = node_filter_cb
        
    def num_nodes(self):
        return len(self.graph.nodes)

    def num_edges(self):
        return len(self.graph.nodes) * len(self.graph.nodes)
    
    def generate_graph(self, connectivity):
        if connectivity is None:
            return None
        return create_graph_from_tvb(connectivity)

    def onclick_node(self, node):
        pass
            
    def create_graph_render(self, connectivity):
        return go.FigureWidget()

    def set_title(self, title):
        l = self.display.layout
        l.title = title
        self.display.layout = l

    def gen_data(self, connectivity):
        pass

    def gen_trace_edges(self, connectivity):
        pass

    @func_timer
    def apply_sig_nodes(self, sig_nodes, snap_idx):
        print(f"sig nodes{sig_nodes}")
        if sig_nodes == None:
            return
        symbols = ['circle'] * self.num_nodes()
        for idx, node in enumerate(sig_nodes):
            if node > 0:
                # rising
                if snap_idx >= node:
                    symbols[idx] = 'cross'
            if node < 0:
                if snap_idx >= node:
                    symbols[idx] = 'square'
        self.display.update_traces(marker=dict(symbol=symbols, size=15), selector=dict(name='nodes'))
        
    
    @func_timer
    def apply_node_colorscheme(self, metric_res, snap_idx):
        if metric_res == None:
            return
        colors = [0] * self.num_edges()
        for (a,b) in metric_res[snap_idx]:
                colors[a] = b  
        self.display.update_traces(marker=dict(color=colors, colorscale='Viridis'), selector=dict(name='nodes'))
        
    def apply_node_labels(self, metric_res, snap_idx):
        if metric_res == None:
            return
        labels = copy.deepcopy(self.node_labels)
        for (a,b) in metric_res[snap_idx]:
            labels[a] = labels[a]+f"\n{b}"
        self.display.update_traces(text=labels, selector=dict(name='nodes'))
            