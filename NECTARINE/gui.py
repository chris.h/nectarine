from ipywidgets import widgets
from NECTARINE.ipyfilechooser import FileChooser
import os
import pickle
import copy

from NECTARINE.import_data_widget import Import_Data_Widget
from NECTARINE.analysis_widget import AnalysisWidget
from NECTARINE.save_load_widget import Save_Load_Widget
from tvb.simulator import lab

from NECTARINE.NeuroDP_framework import Patient



class Gui(object):
        
    def __init__(self):
        self.import_data = Import_Data_Widget(self.on_import_cb)
        self.save_load_tab = Save_Load_Widget(load_cb=self.on_load, save_cb=self.on_save)
        tabs = [self.import_data.display, self.save_load_tab.display]
        self.display = widgets.Tab(tabs)
        self.display.set_title(0, 'Data Import')
        self.display.set_title(1, "Load and Save State")
        self.display.set_title(2, "Load and Save State")
        


    def on_import_cb(self, patient, connectivity):
        if len(self.display.children) > 2:
            #widget has been created already
            return
        self.analysis_widget = AnalysisWidget(patient, connectivity, self.update_out_cb)
        children = list(self.display.children)
        children.insert(1, self.analysis_widget.display)
        self.display.children = children
        self.display.set_title(1, "Analysis")
        # enable saving state now
        self.save_load_tab.enable_save_widget()
        


    def on_save(self):
        # this is the callback that gets called when the user wants to save state
        # 1. gater all data that needs to be saved
        save_state = {
            'patient' : copy.deepcopy(self.analysis_widget.patient),
            'connectivity' : copy.deepcopy(self.analysis_widget.connectivity),
            'node_filter' : copy.deepcopy(self.analysis_widget.curr_node_filter),
            'edge_filter' : copy.deepcopy(self.analysis_widget.curr_edge_filter),
            'metric_res' : copy.deepcopy(self.analysis_widget.curr_met_res),
            'sig_nodes' : copy.deepcopy(self.analysis_widget.sig_nodes),
            'FC_graphs' : copy.deepcopy(self.analysis_widget.brain_graphs),
            'current_snap' : copy.deepcopy(self.analysis_widget.current_snapshot)
        }
        return save_state
        
        
    def on_load(self, save_state):
        # gets called when the user loads a saved state, this repaces the analysis widget if it already exists
        self.analysis_widget = AnalysisWidget(None, None, self.update_out_cb, save_state=save_state)
        children = list(self.display.children)
        children.insert(1, self.analysis_widget.display)
        self.display.children = children
        self.display.set_title(1, "Analysis")
         # enable saving state now
        self.save_load_tab.enable_save_widget()
        
        
    def update_out_cb(self, out):
        self.out = out