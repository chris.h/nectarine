import ipywidgets as widgets
from NECTARINE.ipyfilechooser import FileChooser
from NECTARINE.util import patient_from_import


class Import_Data_Widget():
    def __init__(self, on_import_cb):
        self.on_import_cb = on_import_cb
        self.patient_id="CBS05"
        self.sim_dir = ""
        self.num_regions = 0
        self.input_regions = self.input_regions()
        self.snapshots = {}
        self.snap_selectors = widgets.VBox([self.select_snapshot(0)])
        self.add_snap_chooser = self.add_snap_chooser()
        self.extra_data = {}
        # self.extra_data_boxes = widgets.VBox([self.add_extra_data(0)])
        # self.add_data_chooser = self.add_data_chooser()
        self.sim_dir_chooser=self.choose_sim_directory()
        
        self.display = widgets.VBox([
                                    self.input_regions,
                                    self.snap_selectors,
                                    self.add_snap_chooser,
                                    # self.extra_data_boxes,
                                    # self.add_data_chooser,
                                    self.sim_dir_chooser,
                                    self.build_graph()]
                                   )
    def input_regions(self):
        reg_input = widgets.IntText(value=0,
                                    step=1,
                                    description='Number of Regions:',
                                    disabled=False)
        def on_input(change):
            self.num_regions = change.new
        reg_input.observe(on_input, names='value')
        return reg_input

    
    def select_snapshot(self, num):
        snap_chooser = FileChooser()
        def on_sel_snap(chooser):
            file = chooser.selected
            self.snapshots[num+1] = file
        snap_chooser.register_callback(on_sel_snap)
        l = widgets.Label(f"select SC snapshot #{num+1}")
        return widgets.VBox([l, snap_chooser])
    
    def add_snap_chooser(self):
        add_snap = widgets.Button(description='add another snapshot')
        def on_add(arg):
            snap_sel_children = self.snap_selectors.children
            num_children = len(snap_sel_children)
            new_select_snapshot = self.select_snapshot(num_children)
            new_children = list(snap_sel_children)
            new_children.append(new_select_snapshot)
            self.snap_selectors.children=new_children        
        add_snap.on_click(on_add)
        return add_snap

    def add_extra_data(self, num):
        n_input = widgets.Text(description="Additional data:", value = "", placeholder="column name")
        t_input = widgets.Combobox(description="Data type:", options=['Text', 'Float', 'List of Floats'], placeholder="choose a type")
        self.extra_data[num] = {'name': '',
                                'typ':''}
        def on_n_input(change):
            self.extra_data[num]['name'] = change.new
                
        def on_t_input(change):
            self.extra_data[num]['typ'] = change.new
        n_input.observe(on_n_input, names='value')
        t_input.observe(on_t_input, names='value')
        return widgets.VBox([n_input, t_input])
    
    def add_data_chooser(self):
        add_data = widgets.Button(description='add additional data fields')
        def on_add(arg):
            data_sel_children = self.extra_data_boxes.children
            num_children = len(data_sel_children)
            new_add_data = self.add_extra_data(num_children)
            new_children = list(data_sel_children)
            new_children.append(new_add_data)
            self.extra_data_boxes.children=new_children
            
        add_data.on_click(on_add)
        return add_data
    
    def choose_sim_directory(self):
        l = widgets.Label("simulation directory")
        i_model_name = widgets.Text(description="Model name", placeholder="eg. rww_v2")
        def on_model_name(change):
            self.model_name = change.new
            print(self.model_name)
        i_model_name.observe(on_model_name, names='value')
        sdir = FileChooser()
        sdir.show_only_dirs = True
        def on_sel_dir(chooser):
            self.sim_dir = chooser.selected_path
        sdir.register_callback(on_sel_dir)
        return widgets.VBox([i_model_name, widgets.HBox([l, sdir])])
    
    def build_graph(self):
        b = widgets.Button(description='build graph!')
        def click(arg):
            print(self.snapshots)
            self.patient, self.connectivity = patient_from_import(p_id=self.patient_id, regions=self.num_regions, snapshots=self.snapshots, extra_data=self.extra_data, model=self.model_name, sim_path='./test_run/simulations/')
            self.on_import_cb(self.patient, self.connectivity)
            
        b.on_click(click)
        return b
            